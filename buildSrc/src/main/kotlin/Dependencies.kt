object Dependencies {

    private object Version {

        const val kotlin: String = "1.4.30"

        const val junit: String = "4.12"
        const val mockito: String = "2.23.4"
        const val mockitoKotlin: String = "2.2.0"
        const val mockitoAndroid: String = "2.22.0"
        const val assertJ: String = "3.18.1"
        const val androidCoreTesting: String = "1.1.1"
        const val androidXTestCore: String = "1.0.0"
        const val androidXJunit: String = "1.1.2"
        const val androidXEspresso: String = "3.3.0"

        const val koin: String = "2.2.2"

        const val okHttpPlatform: String = "4.9.0"
        const val retrofit: String = "2.9.0"
        const val jacksonKotlin: String = "2.10.2"

        const val rxJava2: String = "2.2.20"
        const val rxJavaAndroid2: String = "2.1.1"

        const val androidXCore: String = "1.3.2"
        const val androidXAppCompat: String = "1.2.0"
        const val androidXActivity: String = "1.2.1"
        const val androidXFragment: String = "1.3.1"
        const val androidXWorkManager: String = "2.5.0"
        const val material: String = "1.3.0"

        const val picasso: String = "2.71828"
    }

    object Kotlin {
        const val kotlinStdLib: String = "org.jetbrains.kotlin:kotlin-stdlib:${Version.kotlin}"
    }

    object Test {
        const val junit: String = "junit:junit:4.12"
        const val mockitoCore: String = "org.mockito:mockito-core:${Version.mockito}"
        const val mockitoInline: String = "org.mockito:mockito-inline:${Version.mockito}"
        const val mockitoKotlin: String =
            "com.nhaarman.mockitokotlin2:mockito-kotlin:${Version.mockitoKotlin}"
        const val mockitoAndroid: String = "org.mockito:mockito-android:${Version.mockitoAndroid}"
        const val assertJ: String = "org.assertj:assertj-core:${Version.assertJ}"

        const val androidCoreTesting: String =
            "android.arch.core:core-testing:${Version.androidCoreTesting}"
        const val androidXTestCore: String = "androidx.test:core:${Version.androidXTestCore}"
        const val androidXJunit: String = "androidx.test.ext:junit:${Version.androidXJunit}"
        const val androidXEspresso: String =
            "androidx.test.espresso:espresso-core:${Version.androidXEspresso}"
        const val androidXEspressoIdling: String =
            "androidx.test.espresso:espresso-idling-resource:${Version.androidXEspresso}"
    }

    object Rx {
        const val rxJava2: String = "io.reactivex.rxjava2:rxjava:${Version.rxJava2}"
        const val rxJavaAndroid2: String =
            "io.reactivex.rxjava2:rxandroid:${Version.rxJavaAndroid2}"
    }

    object Koin {
        const val koinAndroid: String = "org.koin:koin-android:${Version.koin}"
        const val koinScopeAndroid: String = "org.koin:koin-androidx-scope:${Version.koin}"
        const val koinViewModel: String = "org.koin:koin-androidx-viewmodel:${Version.koin}"
        const val koinTest: String = "org.koin:koin-test:${Version.koin}"
    }

    object Network {
        const val okHttpPlatform: String =
            "com.squareup.okhttp3:okhttp-bom:${Version.okHttpPlatform}"
        const val okHttp: String = "com.squareup.okhttp3:okhttp"
        const val okHttpInterceptor: String = "com.squareup.okhttp3:logging-interceptor"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
        const val retrofitJacksonConverter =
            "com.squareup.retrofit2:converter-jackson:${Version.retrofit}"
        const val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Version.retrofit}"
        const val jacksonKotlin: String =
            "com.fasterxml.jackson.module:jackson-module-kotlin:${Version.jacksonKotlin}"
        const val retrofitScalarsConverter: String =
            "com.squareup.retrofit2:converter-scalars:${Version.retrofit}"
    }

    object Android {

        const val androidXCore: String = "androidx.core:core-ktx:${Version.androidXCore}"
        const val androidXAppCompat: String =
            "androidx.appcompat:appcompat:${Version.androidXAppCompat}"
        const val androidXActivity: String =
            "androidx.activity:activity-ktx:${Version.androidXActivity}"
        const val androidXFragment: String =
            "androidx.fragment:fragment-ktx:${Version.androidXFragment}"
        const val androidXWorkManager: String =
            "androidx.work:work-runtime:${Version.androidXWorkManager}"
        const val androidXWorkManagerRx2: String =
            "androidx.work:work-rxjava2:${Version.androidXWorkManager}"
        const val material: String = "com.google.android.material:material:${Version.material}"
    }

    object Utils {
        const val picasso: String = "com.squareup.picasso:picasso:${Version.picasso}"
    }
}