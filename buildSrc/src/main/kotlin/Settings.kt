import org.gradle.api.Project

private const val DEPLOY_URL_KEY: String = "deployUrl"

fun Project.deployUrl(fallbackUrl: String = "http://localhost/"): String {

    return if (hasProperty(DEPLOY_URL_KEY)) {

        properties[DEPLOY_URL_KEY].toString()
    } else {

        fallbackUrl
    }
}