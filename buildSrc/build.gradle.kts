plugins {
    java
    kotlin("jvm") version "1.4.21"
}

buildscript {

    repositories {
        mavenCentral()
        jcenter()
        maven {
            url = uri("https://maven.google.com")
        }
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.21")
    }
}

group = "aspectjak"
version = "1.0.0"

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://maven.google.com")
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit", "junit", "4.12")
}