package com.example.spark

import android.os.StrictMode
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.example.spark.foundation.Dispatchers
import com.example.spark.profile.City
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Profile
import com.example.spark.profile.internal.ProfileApiService
import com.example.spark.ui.MainActivity
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.test.KoinTest
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.util.Date
import java.util.UUID

class GenderPickTest : KoinTest {

    private val mockProfileApiService: ProfileApiService = mock()

    private val testModule = module {
        single(override = true) {
            mockProfileApiService
        } bind ProfileApiService::class
        single(override = true) {
            object : Dispatchers {
                override fun io(): Scheduler = AndroidSchedulers.mainThread()

                override fun computation(): Scheduler = AndroidSchedulers.mainThread()

                override fun ui(): Scheduler = AndroidSchedulers.mainThread()
            }
        } bind Dispatchers::class
    }

    private val profile = profile()

    @Before
    fun setup() {

        loadKoinModules(testModule)

        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile)))
        )
        whenever(mockProfileApiService.updateProfile(any(), any())).thenReturn(
            Observable.just(Result.response(Response.success(Unit)))
        )
    }

    @After
    fun tearDown() {

        unloadKoinModules(testModule)
    }

    @Test
    fun shouldOpenSelectionDialog() {

        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(generateGenderList(randomNumber()))))
        )

        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.gender)).perform(click())

        onView(withText(getResourceString(R.string.profile_dialog_title_gender))).check(
            matches(
                isDisplayed()
            )
        )

    }

    @Test
    fun shouldUpdateSelectedValue() {

        val genders = generateGenderList(randomNumber())
        val selectedGender = genders.first()

        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(genders)))
        )

        ActivityScenario.launch(MainActivity::class.java)

        onView(withId(R.id.gender)).perform(click())

        onView(withText(getResourceString(R.string.profile_dialog_title_gender)))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        onView(withText(selectedGender.name)).perform(click())

        onView(withId(R.id.gender)).check(matches(withText(selectedGender.name)))
    }
}