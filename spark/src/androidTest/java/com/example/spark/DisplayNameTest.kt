package com.example.spark

import android.os.StrictMode
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.hasErrorText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.spark.foundation.Dispatchers
import com.example.spark.profile.internal.ProfileApiService
import com.example.spark.ui.MainActivity
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import org.hamcrest.CoreMatchers.any
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result

@RunWith(AndroidJUnit4::class)
class DisplayNameTest {

    private val mockProfileApiService: ProfileApiService = mock()

    private val testModule = module {
        single(override = true) {
            mockProfileApiService
        } bind ProfileApiService::class
        single(override = true) {
            object : Dispatchers {
                override fun io(): Scheduler = AndroidSchedulers.mainThread()

                override fun computation(): Scheduler = AndroidSchedulers.mainThread()

                override fun ui(): Scheduler = AndroidSchedulers.mainThread()
            }
        } bind Dispatchers::class
    }

    private val profile = profile()

    @Before
    fun setup() {

        loadKoinModules(testModule)

        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().permitAll().build())

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(com.nhaarman.mockitokotlin2.any())).thenReturn(
            Observable.just(Result.response(Response.success(profile)))
        )
        whenever(mockProfileApiService.updateProfile(
            com.nhaarman.mockitokotlin2.any(),
            com.nhaarman.mockitokotlin2.any()
        )).thenReturn(
            Observable.just(Result.response(Response.success(Unit)))
        )
    }

    @After
    fun tearDown() {

        unloadKoinModules(testModule)
    }

    @Test
    fun shouldShowErrorIfEmpty() {

        launch(MainActivity::class.java)

        onView(withId(R.id.displayName)).perform(clearText(), closeSoftKeyboard())

        onView(withId(R.id.done)).perform(click())

        onView(withId(R.id.displayName)).check(matches(hasErrorText(getResourceString(R.string.display_name_empty))))
    }

    @Test
    fun shouldNotShowErrorIfMatchesRange() {

        val maxLength = getResourceInteger(R.integer.profile_length_display_name)

        launch(MainActivity::class.java)

        onView(withId(R.id.displayName))
            .perform(typeText(randomString(randomNumber(1, maxLength - 1))), closeSoftKeyboard())
        onView(withId(R.id.done)).perform(click())

        onView(withId(R.id.displayName)).check(matches(not(hasErrorText(any(String::class.java)))))
    }

    @Test
    fun shouldNotExceedMaxLength() {

        val maxLength = getResourceInteger(R.integer.profile_length_display_name)

        launch(MainActivity::class.java)

        onView(withId(R.id.displayName))
            .perform(typeText(randomString(maxLength + 1)), closeSoftKeyboard())
        onView(withId(R.id.done)).perform(click())

        onView(withId(R.id.displayName))
            .check(
                matches(
                    withLength(maxLength)
                )
            )
    }
}