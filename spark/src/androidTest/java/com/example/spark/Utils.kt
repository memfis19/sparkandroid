package com.example.spark

import android.content.Context
import android.view.View
import android.widget.EditText
import androidx.annotation.IntegerRes
import androidx.annotation.StringRes
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.platform.app.InstrumentationRegistry
import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Picture
import com.example.spark.profile.Profile
import com.example.spark.profile.Religion
import org.hamcrest.Description
import org.hamcrest.Matcher
import java.util.Base64
import java.util.Date
import java.util.UUID
import kotlin.random.Random

//Android specific
fun getResourceInteger(@IntegerRes id: Int): Int {

    val targetContext: Context = InstrumentationRegistry.getInstrumentation().targetContext
    return targetContext.resources.getInteger(id)
}

fun getResourceString(@StringRes id: Int): String {

    val targetContext: Context = InstrumentationRegistry.getInstrumentation().targetContext
    return targetContext.resources.getString(id)
}


//Matchers
fun withLength(length: Int): Matcher<View> {

    return object : BoundedMatcher<View, EditText>(EditText::class.java) {

        override fun describeTo(description: Description) {

            description.appendText("")
        }

        override fun matchesSafely(view: EditText): Boolean {
            return view.editableText.toString().length == length
        }
    }
}

//Functions for testing

fun profile(
    id: Long? = randomNumber().toLong(),
    displayName: String = randomString(randomNumber()),
    realName: String = randomString(randomNumber()),
    picture: Picture? = picture(),
    birthdate: Date = Date(),
    height: Double = randomNumber().toDouble(),
    gender: Gender = generateGenderList(1).first(),
    maritalStatus: MaritalStatus = generateMaritalStatusList(1).first(),
    city: City = generateCityList(1).first(),
    religion: Religion = generateReligionList(1).first(),
    figure: Figure = generateFiguresList(1).first(),
    occupation: String = randomString(randomNumber()),
    aboutMe: String = randomString(randomNumber())
): Profile {

    return Profile(
        id = id,
        displayName = displayName,
        realName = realName,
        height = height,
        picture = picture,
        birthdate = birthdate,
        gender = gender,
        maritalStatus = maritalStatus,
        location = city,
        religion = religion,
        figure = figure,
        occupation = occupation,
        aboutMe = aboutMe
    )
}

fun picture(): Picture {

    return Picture(
        guid = Base64.getEncoder().encodeToString(UUID.randomUUID().toString().toByteArray(Charsets.UTF_8)),
        mimeType = "image/png"
    )
}

fun generateReligionList(size: Int): List<Religion> {

    return generateSequence {
        Religion(
            id = UUID.randomUUID().toString(),
            name = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun generateMaritalStatusList(size: Int): List<MaritalStatus> {

    return generateSequence {
        MaritalStatus(
            id = UUID.randomUUID().toString(),
            name = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun generateGenderList(size: Int): List<Gender> {

    return generateSequence {
        Gender(
            id = UUID.randomUUID().toString(),
            name = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun generateFiguresList(size: Int): List<Figure> {

    return generateSequence {
        Figure(
            id = UUID.randomUUID().toString(),
            name = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun generateEthnicityList(size: Int): List<Ethnicity> {

    return generateSequence {
        Ethnicity(
            id = UUID.randomUUID().toString(),
            name = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun generateCityList(size: Int): List<City> {

    var index: Long = 0
    return generateSequence {
        City(
            id = index++,
            city = UUID.randomUUID().toString(),
            latitude = UUID.randomUUID().toString(),
            longitude = UUID.randomUUID().toString()
        )
    }.take(size).toList()
}

fun randomNumber(from: Int = 1, to: Int = 100): Int {

    return Random.nextInt(from, to)
}

fun randomString(length: Int): String {

    val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    return (1..length)
        .map { Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("");
}