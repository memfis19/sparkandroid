package com.example.spark.profile.internal

import com.example.spark.configuration.HttpStatus
import com.example.spark.profile.error.BadRequest
import com.example.spark.profile.error.NotExist
import com.example.spark.profile.error.NotValid
import org.junit.Assert.assertEquals
import org.junit.Test

class ResultUnboxerTest {

    @Test
    fun `should return BadRequest when BAD_REQUEST(400)`() {

        val exception = handleHttpCode(HttpStatus.BAD_REQUEST)
        assertEquals(exception, BadRequest)
    }

    @Test
    fun `should return NotExist when NOT_FOUND(404)`() {

        val exception = handleHttpCode(HttpStatus.NOT_FOUND)
        assertEquals(exception, NotExist)
    }

    @Test
    fun `should return NotValid when CONFLICT(409)`() {

        val exception = handleHttpCode(HttpStatus.CONFLICT)
        assertEquals(exception, NotValid)
    }
}