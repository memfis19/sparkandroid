package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.Religion
import com.example.spark.profile.interactor.GetReligions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetReligionTest : BaseTest() {

    private val sut: GetReligions by inject()

    @Test
    fun `should return empty list of religions if no errors`() {

        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<Religion>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { religions -> religions.isEmpty() }
    }

    @Test
    fun `should return list of religions if no errors and match size to response`() {

        val religionsCount: Int = randomNumber(10, 1000)
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(
                success(
                    generateReligionList(
                        religionsCount
                    )
                )
            )
        )

        val observer = TestObserver<List<Religion>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { religions -> religions.size == religionsCount }
    }
}