package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.City
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Profile
import com.example.spark.profile.error.NotExist
import com.example.spark.profile.interactor.GetProfile
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.Date
import java.util.UUID
import kotlin.random.Random

class GetProfileTest : BaseTest() {

    private val sut: GetProfile by inject()

    @Test
    fun `should return profile if no errors`() {

        val profileId: Long = Random.nextLong(1, 1000)
        val cityId: Long = Random.nextLong(1, 1000)

        val profile = Profile(
            id = profileId,
            displayName = "",
            realName = "",
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(success(profile))
        )

        val observer = TestObserver<Profile>()
        sut.execute(profileId).subscribe(observer)

        observer.await()
        observer.assertNoErrors()
    }

    @Test
    fun `should return profile NotExist if wrong ID`() {

        val profileId: Long = Random.nextLong(1, 1000)

        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(error(404))
        )

        val observer = TestObserver<Profile>()
        sut.execute(profileId).subscribe(observer)

        observer.await()
        observer.assertError(NotExist::class.java)
    }
}