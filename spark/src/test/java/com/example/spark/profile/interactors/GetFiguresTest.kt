package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.Figure
import com.example.spark.profile.interactor.GetFigures
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetFiguresTest : BaseTest() {

    private val sut: GetFigures by inject()

    @Test
    fun `should return empty list of figures if no errors`() {

        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<Figure>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { figures -> figures.isEmpty() }
    }

    @Test
    fun `should return list of figures if no errors and match size to response`() {

        val figuresCount: Int = randomNumber(5, 200)

        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(
                success(
                    generateFiguresList(
                        figuresCount
                    )
                )
            )
        )

        val observer = TestObserver<List<Figure>>()

        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { figures -> figures.size == figuresCount }
    }
}