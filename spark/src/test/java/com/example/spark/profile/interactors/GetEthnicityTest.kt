package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.interactor.GetEthnicities
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetEthnicityTest : BaseTest() {

    private val sut: GetEthnicities by inject()

    @Test
    fun `should return empty list if no errors`() {

        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<Ethnicity>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { figures -> figures.isEmpty() }
    }

    @Test
    fun `should return list if no errors and match size to response`() {

        val count: Int = randomNumber(5, 200)

        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(
                success(
                    generateEthnicityList(
                        count
                    )
                )
            )
        )

        val observer = TestObserver<List<Ethnicity>>()

        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { result -> result.size == count }
    }
}