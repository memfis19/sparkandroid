package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.Gender
import com.example.spark.profile.interactor.GetGenders
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetGendersTest : BaseTest() {

    private val sut: GetGenders by inject()

    @Test
    fun `should return empty list of genders if no errors`() {

        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<Gender>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { genders -> genders.isEmpty() }
    }

    @Test
    fun `should return list of genders if no errors and match size to response`() {

        val gendersCount: Int = randomNumber(10, 1000)

        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(
                success(generateGenderList(
                    gendersCount
                ))
            )
        )

        val observer = TestObserver<List<Gender>>()

        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { genders -> genders.size == gendersCount }
    }
}