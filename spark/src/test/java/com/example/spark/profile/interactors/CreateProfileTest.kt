package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.City
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Profile
import com.example.spark.profile.error.BadRequest
import com.example.spark.profile.interactor.CreateProfile
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.Date
import java.util.UUID
import kotlin.random.Random

class CreateProfileTest : BaseTest() {

    private val sut: CreateProfile by inject()

    @Test
    fun `should return ID if create has been success`() {

        val profileId: Long = Random.nextLong(1, 10000)
        val cityId: Long = Random.nextLong(1, 10000)
        val profile: Profile = Profile(
            displayName = UUID.randomUUID().toString(),
            realName = UUID.randomUUID().toString(),
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )

        whenever(mockProfileApiService.createProfile(profile)).thenReturn(
            Observable.just(success(profileId))
        )

        val observer = TestObserver<Long>()
        sut.execute(profile).subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { id -> id == profileId }
    }

    @Test
    fun `should return BadRequest(400) if create has not been success`() {

        val cityId: Long = Random.nextLong(1, 10000)
        val profile: Profile = Profile(
            displayName = UUID.randomUUID().toString(),
            realName = UUID.randomUUID().toString(),
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )

        whenever(mockProfileApiService.createProfile(profile)).thenReturn(
            Observable.just(error(400))
        )

        val observer = TestObserver<Long>()
        sut.execute(profile).subscribe(observer)

        observer.await()
        observer.assertError(BadRequest::class.java)
    }
}