package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.interactor.GetMaritalStatuses
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetMaritalStatusTest : BaseTest() {

    private val sut: GetMaritalStatuses by inject()

    @Test
    fun `should return empty list if no errors`() {

        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<MaritalStatus>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { genders -> genders.isEmpty() }
    }

    @Test
    fun `should return list if no errors and match size to response`() {

        val count: Int = randomNumber(10, 1000)
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(
                success(
                    generateMaritalStatusList(
                        count
                    )
                )
            )
        )

        val observer = TestObserver<List<MaritalStatus>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { result -> result.size == count }
    }
}