package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.City
import com.example.spark.profile.interactor.GetCities
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.UUID

class GetCitiesTest : BaseTest() {

    private val sut: GetCities by inject()

    @Test
    fun `should return empty list of cities if no errors`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(
                success(emptyList())
            )
        )

        val observer = TestObserver<List<City>>()
        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { cities -> cities.isEmpty() }
    }

    @Test
    fun `should return list of cities if no errors and match size to response`() {

        val citiesCount: Int = randomNumber(1, 100)

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(
                success(generateCityList(citiesCount))
            )
        )

        val observer = TestObserver<List<City>>()

        sut.execute().subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { cities -> cities.size == citiesCount }
    }
}