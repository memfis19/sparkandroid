package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.configuration.HttpStatus
import com.example.spark.profile.City
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Profile
import com.example.spark.profile.error.BadRequest
import com.example.spark.profile.error.NotValid
import com.example.spark.profile.interactor.UpdateProfile
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.test.inject
import java.util.Date
import java.util.UUID
import kotlin.random.Random

class UpdateProfileTest : BaseTest() {

    private val sut: UpdateProfile by inject()

    @Test
    fun `should not be any error if update has been success`() {

        val profileId: Long = Random.nextLong(1, 10000)
        val cityId: Long = Random.nextLong(1, 10000)
        val profile: Profile = Profile(
            id = profileId,
            displayName = UUID.randomUUID().toString(),
            realName = UUID.randomUUID().toString(),
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )

        whenever(mockProfileApiService.updateProfile(profileId, profile)).thenReturn(
            Observable.just(success(Unit))
        )

        val observer = TestObserver<Unit>()
        sut.execute(profileId, profile).subscribe(observer)

        observer.await()
        observer.assertNoErrors()
    }

    @Test
    fun `should return BadRequest(400) if update has not been success`() {

        val cityId: Long = Random.nextLong(1, 10000)
        val profileId: Long = Random.nextLong(1, 10000)
        val profile: Profile = Profile(
            id = profileId,
            displayName = UUID.randomUUID().toString(),
            realName = UUID.randomUUID().toString(),
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )

        whenever(mockProfileApiService.updateProfile(profileId, profile)).thenReturn(
            Observable.just(error(HttpStatus.BAD_REQUEST))
        )

        val observer = TestObserver<Unit>()
        sut.execute(profileId, profile).subscribe(observer)

        observer.await()
        observer.assertError(BadRequest::class.java)
    }

    @Test
    fun `should return NotValid(409) if update has invalid attributes`() {

        val cityId: Long = Random.nextLong(1, 10000)
        val profileId: Long = Random.nextLong(1, 10000)
        val profile: Profile = Profile(
            id = profileId,
            displayName = UUID.randomUUID().toString(),
            realName = UUID.randomUUID().toString(),
            birthdate = Date(),
            gender = Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            maritalStatus = MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            ),
            location = City(
                id = cityId,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        )

        whenever(mockProfileApiService.updateProfile(profileId, profile)).thenReturn(
            Observable.just(error(HttpStatus.CONFLICT))
        )

        val observer = TestObserver<Unit>()
        sut.execute(profileId, profile).subscribe(observer)

        observer.await()
        observer.assertError(NotValid::class.java)
    }
}