package com.example.spark.profile.interactors

import com.example.spark.BaseTest
import com.example.spark.profile.Picture
import com.example.spark.profile.error.BadRequest
import com.example.spark.profile.interactor.UpdateProfilePicture
import com.example.spark.profile.upload.PictureUploader
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Test
import org.koin.core.module.Module
import org.koin.dsl.module
import org.koin.test.inject
import java.io.File

class UpdateProfilePictureTest : BaseTest() {

    private val sut: UpdateProfilePicture by inject()
    private val pictureUploader: PictureUploader by inject()

    override fun testModules(): List<Module> {
        return listOf(module {
            single(override = true) {
                mock<PictureUploader>()
            }
        })
    }

    @Test
    fun `should return BadRequest if failed to upload()`() {

        whenever(pictureUploader.upload(any(), any(), any())).thenReturn(
            Observable.error(BadRequest)
        )

        val observer = TestObserver<Picture>()
        sut.execute(randomNumber().toLong(), File(""), "image/png").subscribe(observer)

        observer.await()
        observer.assertError(BadRequest::class.java)
    }

    @Test
    fun `should return picture if upload() has been successful`() {

        val picture = picture()
        whenever(pictureUploader.upload(any(), any(), any())).thenReturn(
            Observable.just(picture)
        )

        val observer = TestObserver<Picture>()
        sut.execute(randomNumber().toLong(), File(""), "image/png").subscribe(observer)

        observer.await()
        observer.assertNoErrors()
        observer.assertValue { actual -> actual == picture }
    }

}