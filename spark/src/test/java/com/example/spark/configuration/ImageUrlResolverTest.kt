package com.example.spark.configuration

import com.example.spark.BaseTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.koin.test.inject
import java.net.URI
import java.util.Base64
import java.util.UUID

class ImageUrlResolverTest : BaseTest() {

    private val sut: ImageUrlResolver by inject()
    private val networkConfiguration: NetworkConfiguration by inject()

    @Test
    fun `should return correct url`() {

        val guid = Base64.getEncoder()
            .encodeToString(UUID.randomUUID().toString().toByteArray(Charsets.UTF_8))

        val expected = networkConfiguration.baseUrl() + "pictures/$guid"
        val url = sut.resolveUrl(guid)

        assertEquals(expected, url)
    }

    @Test
    fun `should return valid url`() {

        val guid = Base64.getEncoder()
            .encodeToString(UUID.randomUUID().toString().toByteArray(Charsets.UTF_8))

        val expected = networkConfiguration.baseUrl() + "pictures/$guid"
        val url = sut.resolveUrl(guid)
        val uri = URI.create(url)

        assertEquals(expected, uri.toString())
    }
}