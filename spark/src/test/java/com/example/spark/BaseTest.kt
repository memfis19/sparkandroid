package com.example.spark

import android.content.Context
import com.example.spark.di.sparkModule
import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Picture
import com.example.spark.profile.Profile
import com.example.spark.profile.Religion
import com.example.spark.profile.internal.ProfileApiService
import okhttp3.internal.http.RealResponseBody
import okio.Buffer
import okio.BufferedSource
import okio.ByteString
import okio.Options
import okio.Sink
import okio.Timeout
import org.junit.Rule
import org.koin.android.ext.koin.androidContext
import org.koin.core.logger.Level
import org.koin.core.module.Module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import org.mockito.Mockito
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.io.InputStream
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.util.Base64
import java.util.Date
import java.util.UUID
import kotlin.random.Random

abstract class BaseTest : KoinTest {

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger(Level.DEBUG)
        androidContext(Mockito.mock(Context::class.java))
        modules(listOf(sparkModule, testProfileModule) + testModules())
    }

    internal val mockProfileApiService: ProfileApiService by inject()

    protected open fun testModules(): List<Module> {

        return emptyList()
    }

    protected fun <T> success(value: T): Result<T> {

        return Result.response(Response.success(value))
    }

    protected fun <T> error(error: Throwable): Result<T> {

        return Result.error(error)
    }

    protected fun <T> error(code: Int): Result<T> {

        return Result.response(
            Response.error<T>(
                code,
                RealResponseBody(null, 0, FakeBufferedSource)
            )
        )
    }

    protected fun randomNumber(from: Int = 0, to: Int = 100): Int {

        return Random.nextInt(from, to)
    }

    protected fun randomString(length: Int): String {

        val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        return (1..length)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("");
    }

    protected fun profile(
        id: Long? = randomNumber().toLong(),
        displayName: String = randomString(randomNumber()),
        realName: String = randomString(randomNumber()),
        picture: Picture? = picture(),
        birthdate: Date = Date(),
        height: Double = randomNumber().toDouble(),
        gender: Gender = generateGenderList(1).first(),
        maritalStatus: MaritalStatus = generateMaritalStatusList(1).first(),
        city: City = generateCityList(1).first(),
        religion: Religion = generateReligionList(1).first(),
        figure: Figure = generateFiguresList(1).first(),
        occupation: String = randomString(randomNumber()),
        aboutMe: String = randomString(randomNumber())
    ): Profile {

        return Profile(
            id = id,
            displayName = displayName,
            realName = realName,
            height = height,
            picture = picture,
            birthdate = birthdate,
            gender = gender,
            maritalStatus = maritalStatus,
            location = city,
            religion = religion,
            figure = figure,
            occupation = occupation,
            aboutMe = aboutMe
        )
    }

    protected fun picture(): Picture {

        return Picture(
            guid = Base64.getEncoder().encodeToString(UUID.randomUUID().toString().toByteArray(Charsets.UTF_8)),
            mimeType = "image/png"
        )
    }

    protected fun generateReligionList(size: Int): List<Religion> {

        return generateSequence {
            Religion(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected fun generateMaritalStatusList(size: Int): List<MaritalStatus> {

        return generateSequence {
            MaritalStatus(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected fun generateGenderList(size: Int): List<Gender> {

        return generateSequence {
            Gender(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected fun generateFiguresList(size: Int): List<Figure> {

        return generateSequence {
            Figure(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected fun generateEthnicityList(size: Int): List<Ethnicity> {

        return generateSequence {
            Ethnicity(
                id = UUID.randomUUID().toString(),
                name = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected fun generateCityList(size: Int): List<City> {

        var index: Long = 0
        return generateSequence {
            City(
                id = index++,
                city = UUID.randomUUID().toString(),
                latitude = UUID.randomUUID().toString(),
                longitude = UUID.randomUUID().toString()
            )
        }.take(size).toList()
    }

    protected object FakeBufferedSource : BufferedSource {
        override val buffer: Buffer
            get() = TODO("Not yet implemented")

        override fun buffer(): Buffer {
            TODO("Not yet implemented")
        }

        override fun close() {
            TODO("Not yet implemented")
        }

        override fun exhausted(): Boolean {
            TODO("Not yet implemented")
        }

        override fun indexOf(b: Byte): Long {
            TODO("Not yet implemented")
        }

        override fun indexOf(b: Byte, fromIndex: Long): Long {
            TODO("Not yet implemented")
        }

        override fun indexOf(b: Byte, fromIndex: Long, toIndex: Long): Long {
            TODO("Not yet implemented")
        }

        override fun indexOf(bytes: ByteString): Long {
            TODO("Not yet implemented")
        }

        override fun indexOf(bytes: ByteString, fromIndex: Long): Long {
            TODO("Not yet implemented")
        }

        override fun indexOfElement(targetBytes: ByteString): Long {
            TODO("Not yet implemented")
        }

        override fun indexOfElement(targetBytes: ByteString, fromIndex: Long): Long {
            TODO("Not yet implemented")
        }

        override fun inputStream(): InputStream {
            TODO("Not yet implemented")
        }

        override fun isOpen(): Boolean {
            TODO("Not yet implemented")
        }

        override fun peek(): BufferedSource {
            TODO("Not yet implemented")
        }

        override fun rangeEquals(offset: Long, bytes: ByteString): Boolean {
            TODO("Not yet implemented")
        }

        override fun rangeEquals(
            offset: Long,
            bytes: ByteString,
            bytesOffset: Int,
            byteCount: Int
        ): Boolean {
            TODO("Not yet implemented")
        }

        override fun read(sink: ByteArray): Int {
            TODO("Not yet implemented")
        }

        override fun read(sink: ByteArray, offset: Int, byteCount: Int): Int {
            TODO("Not yet implemented")
        }

        override fun read(sink: Buffer, byteCount: Long): Long {
            TODO("Not yet implemented")
        }

        override fun read(dst: ByteBuffer?): Int {
            TODO("Not yet implemented")
        }

        override fun readAll(sink: Sink): Long {
            TODO("Not yet implemented")
        }

        override fun readByte(): Byte {
            TODO("Not yet implemented")
        }

        override fun readByteArray(): ByteArray {
            TODO("Not yet implemented")
        }

        override fun readByteArray(byteCount: Long): ByteArray {
            TODO("Not yet implemented")
        }

        override fun readByteString(): ByteString {
            TODO("Not yet implemented")
        }

        override fun readByteString(byteCount: Long): ByteString {
            TODO("Not yet implemented")
        }

        override fun readDecimalLong(): Long {
            TODO("Not yet implemented")
        }

        override fun readFully(sink: ByteArray) {
            TODO("Not yet implemented")
        }

        override fun readFully(sink: Buffer, byteCount: Long) {
            TODO("Not yet implemented")
        }

        override fun readHexadecimalUnsignedLong(): Long {
            TODO("Not yet implemented")
        }

        override fun readInt(): Int {
            TODO("Not yet implemented")
        }

        override fun readIntLe(): Int {
            TODO("Not yet implemented")
        }

        override fun readLong(): Long {
            TODO("Not yet implemented")
        }

        override fun readLongLe(): Long {
            TODO("Not yet implemented")
        }

        override fun readShort(): Short {
            TODO("Not yet implemented")
        }

        override fun readShortLe(): Short {
            TODO("Not yet implemented")
        }

        override fun readString(charset: Charset): String {
            TODO("Not yet implemented")
        }

        override fun readString(byteCount: Long, charset: Charset): String {
            TODO("Not yet implemented")
        }

        override fun readUtf8(): String {
            TODO("Not yet implemented")
        }

        override fun readUtf8(byteCount: Long): String {
            TODO("Not yet implemented")
        }

        override fun readUtf8CodePoint(): Int {
            TODO("Not yet implemented")
        }

        override fun readUtf8Line(): String? {
            TODO("Not yet implemented")
        }

        override fun readUtf8LineStrict(): String {
            TODO("Not yet implemented")
        }

        override fun readUtf8LineStrict(limit: Long): String {
            TODO("Not yet implemented")
        }

        override fun request(byteCount: Long): Boolean {
            TODO("Not yet implemented")
        }

        override fun require(byteCount: Long) {
            TODO("Not yet implemented")
        }

        override fun select(options: Options): Int {
            TODO("Not yet implemented")
        }

        override fun skip(byteCount: Long) {
            TODO("Not yet implemented")
        }

        override fun timeout(): Timeout {
            TODO("Not yet implemented")
        }
    }
}