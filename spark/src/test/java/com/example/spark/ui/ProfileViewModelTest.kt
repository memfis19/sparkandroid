package com.example.spark.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.spark.BaseTest
import com.example.spark.configuration.ImageUrlResolver
import com.example.spark.configuration.SimpleImageUrlResolver
import com.example.spark.profile.error.Ui
import com.example.spark.profile.interactor.CreateProfile
import com.example.spark.profile.interactor.UpdateProfile
import com.example.spark.profile.upload.PictureUploader
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.test.inject
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.io.File
import java.util.Date
import java.util.UUID

@RunWith(MockitoJUnitRunner::class)
class ProfileViewModelTest : BaseTest() {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val sut: ProfileViewModel by inject()
    private val createProfile: CreateProfile by inject()
    private val updateProfile: UpdateProfile by inject()
    private val imageUrlResolver: ImageUrlResolver by inject()
    private val pictureUploader: PictureUploader by inject()

    override fun testModules(): List<Module> {
        return listOf(
            module {
                single(override = true) {
                    spy(UpdateProfile(get(), get()))
                }
                single(override = true) {
                    spy(CreateProfile(get(), get()))
                }
                single(override = true) {
                    spy(SimpleImageUrlResolver(get()))
                } bind ImageUrlResolver::class
                single(override = true) {
                    mock<PictureUploader>()
                }
            }
        )
    }

    @Test
    fun `should have initial state Idle`() {

        assertEquals(ProfileViewModel.State.Idle, sut.state.value)
    }

    @Test
    fun `should update state to Ready when loadProfileWithAttributes is OK`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        assertTrue(sut.state.value is ProfileViewModel.State.Ready.Valid)
    }

    @Test
    fun `should match ViewModel state to loadProfileWithAttributes result`() {

        val cities = generateCityList(randomNumber(1))
        val genders = generateGenderList(randomNumber(1))
        val maritalStatuses = generateMaritalStatusList(randomNumber(1))
        val figures = generateFiguresList(randomNumber(1))
        val religions = generateReligionList(randomNumber(1))
        val ethnicities = generateEthnicityList(randomNumber(1))
        val profile = profile()

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(cities)))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(genders)))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(ethnicities)))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(maritalStatuses)))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(religions)))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(figures)))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile)))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val state = sut.state.value
        assertTrue(sut.state.value is ProfileViewModel.State.Ready.Valid)

        val validState = state as ProfileViewModel.State.Ready.Valid

        assertEquals(profile, validState.profile)

        assertEquals(cities, validState.profileAttributes.cities)
        assertEquals(genders, validState.profileAttributes.genders)
        assertEquals(maritalStatuses, validState.profileAttributes.maritalStatuses)
        assertEquals(religions, validState.profileAttributes.religions)
        assertEquals(ethnicities, validState.profileAttributes.ethnicities)
        assertEquals(figures, validState.profileAttributes.figures)
    }

    @Test
    fun `should updateDisplayName update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newDisplayName = randomString(1)
        sut.updateDisplayName(newDisplayName)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newDisplayName, newState.profile.displayName)
    }

    @Test
    fun `should updateRealName update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newRealName = randomString(1)
        sut.updateRealName(newRealName)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newRealName, newState.profile.realName)
    }

    @Test
    fun `should updateBirthdate update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newBirthdate = Date()
        sut.updateBirthday(newBirthdate)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newBirthdate, newState.profile.birthdate)
    }

    @Test
    fun `should updateCity update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newLocation = generateCityList(1).first()
        sut.updateCity(newLocation)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newLocation, newState.profile.location)
    }

    @Test
    fun `should updateGender update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newGender = generateGenderList(1).first()
        sut.updateGender(newGender)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newGender, newState.profile.gender)
    }

    @Test
    fun `should updateMaritalStatus update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newMs = generateMaritalStatusList(1).first()
        sut.updateMaritalStatus(newMs)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newMs, newState.profile.maritalStatus)
    }

    @Test
    fun `should updateFigure update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newFigure = generateFiguresList(1).first()
        sut.updateFigure(newFigure)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newFigure, newState.profile.figure)
    }

    @Test
    fun `should updateReligion update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newReligion = generateReligionList(1).first()
        sut.updateReligion(newReligion)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newReligion, newState.profile.religion)
    }

    @Test
    fun `should updateEthnicity update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newEthnicity = generateEthnicityList(1).first()
        sut.updateEthnicity(newEthnicity)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newEthnicity, newState.profile.ethnicity)
    }

    @Test
    fun `should updateOccupation update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newOccupation = randomString(1)
        sut.updateOccupation(newOccupation)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newOccupation, newState.profile.occupation)
    }

    @Test
    fun `should updateAboutMe update state`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val newAboutMe = randomString(1)
        sut.updateAboutMe(newAboutMe)

        val newState = sut.state.value as ProfileViewModel.State.Ready
        assertEquals(newAboutMe, newState.profile.aboutMe)
    }

    @Test
    fun `should update state to NotValid with BadDisplayName if displayName isEmpty when applyProfileChanges`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val displayName = ""
        sut.updateDisplayName(displayName)
        sut.applyProfileChanges()

        assertTrue(sut.state.value is ProfileViewModel.State.Ready.NotValid)

        val state = sut.state.value as ProfileViewModel.State.Ready.NotValid
        assertEquals(Ui.BadDisplayName, state.sparkError)
    }

    @Test
    fun `should update state to NotValid with BadRealName if realName isEmpty when applyProfileChanges`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()

        val realName = ""
        sut.updateRealName(realName)
        sut.applyProfileChanges()

        assertTrue(sut.state.value is ProfileViewModel.State.Ready.NotValid)

        val state = sut.state.value as ProfileViewModel.State.Ready.NotValid
        assertEquals(Ui.BadRealName, state.sparkError)
    }

    @Test
    fun `should call updateProfile when applyProfileChanges and valid`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )

        val profile = profile()
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile)))
        )
        whenever(mockProfileApiService.updateProfile(any(), any())).thenReturn(
            Observable.just(Result.response(Response.success(Unit)))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()
        sut.applyProfileChanges()

        verify(updateProfile, atLeastOnce()).execute(any(), any())
    }

    @Test
    fun `should call createProfile when profile id is null and applyProfileChanges`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )

        val profile = profile(id = null)
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile)))
        )
        whenever(mockProfileApiService.createProfile(any())).thenReturn(
            Observable.just(Result.response(Response.success(randomNumber(1).toLong())))
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()
        sut.applyProfileChanges()

        verify(createProfile, atLeastOnce()).execute(any())
    }

    @Test
    fun `should call ImageUrlResolver when resolveUrl`() {

        sut.resolveUrl(UUID.randomUUID().toString())

        verify(imageUrlResolver, atLeastOnce()).resolveUrl(any())
    }

    @Test
    fun `should update state when updateProfilePicture called`() {

        whenever(mockProfileApiService.getCities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getGenders()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getEthnicities()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getMaritalStatuses()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getReligions()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getFigures()).thenReturn(
            Observable.just(Result.response(Response.success(emptyList())))
        )
        whenever(mockProfileApiService.getProfileById(any())).thenReturn(
            Observable.just(Result.response(Response.success(profile())))
        )
        val picture = picture()
        whenever(pictureUploader.upload(any(), any(), any())).thenReturn(
            Observable.just(picture)
        )

        sut.state.observeForever {}
        sut.loadProfileWithAttributes()
        sut.updateProfilePicture(File("/pictures/avatar.png"))

        assertTrue(sut.state.value is ProfileViewModel.State.Ready.Valid)
        val state = sut.state.value as ProfileViewModel.State.Ready.Valid
        assertEquals(picture, state.profile.picture)
    }
}