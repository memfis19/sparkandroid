package com.example.spark

import com.example.spark.foundation.Dispatchers
import com.example.spark.profile.internal.ProfileApiService
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.bind
import org.koin.dsl.module

val testProfileModule = module {

    single(override = true) { TestDispatchers } bind Dispatchers::class
    single(override = true) { mock<ProfileApiService>() }
}

private object TestDispatchers : Dispatchers {

    override fun io(): Scheduler = Schedulers.trampoline()

    override fun computation(): Scheduler = Schedulers.trampoline()

    override fun ui(): Scheduler = Schedulers.trampoline()
}