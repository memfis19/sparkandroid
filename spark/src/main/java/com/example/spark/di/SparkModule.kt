package com.example.spark.di

import com.example.spark.configuration.DefaultNetworkConfiguration
import com.example.spark.configuration.ImageUrlResolver
import com.example.spark.configuration.NetworkConfiguration
import com.example.spark.configuration.NetworkManager
import com.example.spark.configuration.SimpleImageUrlResolver
import com.example.spark.foundation.Dispatchers
import com.example.spark.foundation.RxDispatchers
import com.example.spark.profile.interactor.CreateProfile
import com.example.spark.profile.interactor.GetCities
import com.example.spark.profile.interactor.GetEthnicities
import com.example.spark.profile.interactor.GetFigures
import com.example.spark.profile.interactor.GetGenders
import com.example.spark.profile.interactor.GetMaritalStatuses
import com.example.spark.profile.interactor.GetProfile
import com.example.spark.profile.interactor.GetReligions
import com.example.spark.profile.interactor.LoadProfileAttributes
import com.example.spark.profile.interactor.UpdateProfile
import com.example.spark.profile.interactor.UpdateProfilePicture
import com.example.spark.profile.internal.ProfileApiService
import com.example.spark.profile.upload.PictureUploader
import com.example.spark.profile.upload.WorkManagerPictureUploader
import com.example.spark.ui.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module

val sparkModule = module {

    single { RxDispatchers } bind Dispatchers::class
    single { DefaultNetworkConfiguration } bind NetworkConfiguration::class
    single { NetworkManager(get()) }
    single { get<NetworkManager>().createApiService(ProfileApiService::class.java) }
    single { SimpleImageUrlResolver(get()) } bind ImageUrlResolver::class
    single { WorkManagerPictureUploader(get(), get()) } bind PictureUploader::class

    factory { GetCities(get(), get()) }
    factory { GetGenders(get(), get()) }
    factory { GetFigures(get(), get()) }
    factory { GetEthnicities(get(), get()) }
    factory { GetMaritalStatuses(get(), get()) }
    factory { GetReligions(get(), get()) }
    factory { LoadProfileAttributes(get(), get(), get(), get(), get(), get()) }
    factory { GetProfile(get(), get()) }
    factory { CreateProfile(get(), get()) }
    factory { UpdateProfile(get(), get()) }
    factory { UpdateProfilePicture(get()) }

    viewModel { ProfileViewModel(get(), get(), get(), get(), get(), get()) }
}