package com.example.spark.configuration

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.concurrent.TimeUnit

interface NetworkConfiguration {
    fun baseUrl(): String
    fun timeoutSeconds(): Long
}

class NetworkManager(private val networkConfiguration: NetworkConfiguration) {

    private val retrofit: Retrofit by lazy {

        createRetrofit()
    }

    fun <T> createApiService(apiService: Class<T>): T {

        return retrofit.create(apiService)
    }

    private fun createRetrofit(): Retrofit {

        return Retrofit.Builder()
            .baseUrl(networkConfiguration.baseUrl())
            .client(createOkHttpClient())
            .addCallAdapterFactory(createCallAdapterFactory())
            .addConverterFactory(createConverterFactory())
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
    }

    private fun createConverterFactory(): Converter.Factory {

        return JacksonConverterFactory.create(
            ObjectMapper()
                .setDateFormat(SimpleDateFormat(RFC822MS, Locale.ROOT))
                .configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .registerKotlinModule()
            )
    }

    private fun createCallAdapterFactory(): CallAdapter.Factory {

        return RxJava2CallAdapterFactory.create()
    }

    private fun createOkHttpClient(): OkHttpClient {

        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .writeTimeout(networkConfiguration.timeoutSeconds(), TimeUnit.SECONDS)
            .readTimeout(networkConfiguration.timeoutSeconds(), TimeUnit.SECONDS)
            .connectTimeout(networkConfiguration.timeoutSeconds(), TimeUnit.SECONDS)
            .build()
    }

    companion object {
        const val RFC822MS: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }
}