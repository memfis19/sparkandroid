package com.example.spark.configuration

interface ImageUrlResolver {
    fun resolveUrl(guid: String): String
}

class SimpleImageUrlResolver(private val networkConfiguration: NetworkConfiguration) : ImageUrlResolver {

    override fun resolveUrl(guid: String): String {

        return networkConfiguration.baseUrl() + "pictures/$guid"
    }
}