package com.example.spark.configuration

import com.example.spark.BuildConfig

internal object DefaultNetworkConfiguration : NetworkConfiguration {

    override fun baseUrl(): String = BuildConfig.DEPLOY_URL

    override fun timeoutSeconds(): Long = 60
}