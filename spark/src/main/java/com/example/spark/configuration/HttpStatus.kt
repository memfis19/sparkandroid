package com.example.spark.configuration

object HttpStatus {
    const val OK: Int = 200
    const val BAD_REQUEST: Int = 400
    const val UNAUTHORIZED: Int = 401
    const val FORBIDDEN: Int = 403
    const val NOT_FOUND: Int = 404
    const val CONFLICT: Int = 409
    const val PAYLOAD_TOO_LARGE: Int = 413
}