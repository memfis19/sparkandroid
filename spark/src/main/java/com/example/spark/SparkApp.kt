package com.example.spark

import android.app.Application
import com.example.spark.di.sparkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module

open class SparkApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@SparkApp)
            modules(sparkModules())
        }
    }

    protected open fun sparkModules(): List<Module> {

        return listOf(sparkModule)
    }
}