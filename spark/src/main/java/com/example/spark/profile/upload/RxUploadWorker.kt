package com.example.spark.profile.upload

import android.content.Context
import androidx.work.Data
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.example.spark.profile.internal.ProfileApiService
import com.example.spark.profile.internal.unBox
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.File

@KoinApiExtension
class RxUploadWorker(
    context: Context,
    workerParameters: WorkerParameters
) : RxWorker(context, workerParameters), KoinComponent {

    private val profileApiService: ProfileApiService by inject()

    internal val profileId: Long by lazy {

        inputData.getLong(RxUploadWorker::profileId.name, -1)
    }

    internal val picture: File by lazy {

        File(
            inputData.getString(RxUploadWorker::picture.name)
                ?: throw IllegalArgumentException("Picture is missing.")
        )
    }

    internal val mimeType: String by lazy {

        inputData.getString(RxUploadWorker::mimeType.name)
            ?: throw IllegalArgumentException("MimeType is missing.")
    }

    override fun createWork(): Single<Result> {

        if (profileId < 0) {

            return Single.fromCallable {

                Result.failure()
            }
        }

        val partPicture = MultipartBody.Part.createFormData(
            PART_PICTURE,
            picture.name,
            picture.asRequestBody()
        )

        val partMimeType = MultipartBody.Part.createFormData(PART_MIME_TYPE, mimeType)

        return Single.fromObservable(
            profileApiService.updateProfilePicture(
                profileId,
                partPicture,
                partMimeType
            ).unBox()
        ).map { picture ->

            Result.success(Data.Builder().putAll(picture.toMap()).build())
        }.onErrorReturn { error ->

            Result.failure()
        }
    }

    companion object {

        const val PART_PICTURE: String = "picture"
        const val PART_MIME_TYPE: String = "mimeType"
    }
}