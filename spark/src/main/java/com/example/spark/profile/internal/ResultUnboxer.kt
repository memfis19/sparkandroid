package com.example.spark.profile.internal

import com.example.spark.configuration.HttpStatus
import com.example.spark.profile.error.BadRequest
import com.example.spark.profile.error.GeneralError
import com.example.spark.profile.error.NotExist
import com.example.spark.profile.error.NotValid
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.Result
import java.lang.Exception

fun <T> Observable<Result<T>>.unBox(): Observable<T> {

    return map { result ->

        result.handleResult()
    }
}

internal fun <T> Result<T>.handleResult(): T {

    val responseCode: Int? = response()?.code()
    val responseBody: T? = response()?.body()

    return when {

        responseCode == HttpStatus.OK && responseBody != null -> {

            responseBody
        }

        !isError && responseCode != null && responseCode >= 400 -> {

            throw handleHttpCode(responseCode)
        }

        isError && responseCode != null -> {

            throw handleHttpCode(responseCode)
        }

        isError -> {

            throw error() ?: GeneralError
        }

        else -> {

            throw GeneralError
        }
    }
}

internal fun handleHttpCode(code: Int): Exception {

    return when (code) {
        HttpStatus.BAD_REQUEST -> BadRequest
        HttpStatus.NOT_FOUND -> NotExist
        HttpStatus.CONFLICT -> NotValid
        else -> GeneralError
    }
}