package com.example.spark.profile.upload

import android.content.Context
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.spark.foundation.Dispatchers
import com.example.spark.profile.Picture
import com.example.spark.profile.error.BadRequest
import io.reactivex.Observable
import org.koin.core.component.KoinApiExtension
import java.io.File
import java.util.concurrent.TimeUnit

class WorkManagerPictureUploader(
    private val context: Context,
    private val dispatchers: Dispatchers
) : PictureUploader {

    @KoinApiExtension
    override fun upload(id: Long, file: File, mimeType: String): Observable<Picture> {

        val data = Data.Builder()
            .putLong(RxUploadWorker::profileId.name, id)
            .putString(RxUploadWorker::picture.name, file.path)
            .putString(RxUploadWorker::mimeType.name, mimeType)
            .build()

        val work = OneTimeWorkRequest.Builder(RxUploadWorker::class.java)
            .addTag(file.name)
            .setInputData(data)
            .build()

        WorkManager.getInstance(context).enqueue(work)

        return Observable.interval(1, TimeUnit.SECONDS)
            .map { WorkManager.getInstance(context).getWorkInfoById(work.id).get() }
            .takeUntil { workInfo -> workInfo.isCompleted() }
            .filter { workInfo -> workInfo.isCompleted() }
            .map { workInfo ->

                workInfo.outputData.keyValueMap.let { map ->
                    Picture.fromMap(map)
                } ?: throw BadRequest
            }.subscribeOn(dispatchers.computation()).observeOn(dispatchers.ui())
    }

    private fun WorkInfo.isCompleted(): Boolean {

        return state == WorkInfo.State.SUCCEEDED
            || state == WorkInfo.State.FAILED
            || state == WorkInfo.State.CANCELLED
    }
}