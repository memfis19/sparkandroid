package com.example.spark.profile.interactor

import com.example.spark.foundation.Dispatchers
import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Picture
import com.example.spark.profile.Profile
import com.example.spark.profile.ProfileAttributes
import com.example.spark.profile.Religion
import com.example.spark.profile.internal.ProfileApiService
import com.example.spark.profile.internal.unBox
import com.example.spark.profile.upload.PictureUploader
import io.reactivex.Observable
import java.io.File

class GetCities internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<City>> {

        return profileApiService.getCities()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class GetGenders internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<Gender>> {

        return profileApiService.getGenders()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class GetFigures internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<Figure>> {

        return profileApiService.getFigures()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class GetEthnicities internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<Ethnicity>> {

        return profileApiService.getEthnicities()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class GetMaritalStatuses internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<MaritalStatus>> {

        return profileApiService.getMaritalStatuses()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class GetReligions internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(): Observable<List<Religion>> {

        return profileApiService.getReligions()
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class LoadProfileAttributes internal constructor(
    private val getCities: GetCities,
    private val getGenders: GetGenders,
    private val getFigures: GetFigures,
    private val getEthnicities: GetEthnicities,
    private val getMaritalStatuses: GetMaritalStatuses,
    private val getReligions: GetReligions
) {

    fun execute(): Observable<ProfileAttributes> {

        return Observable.zip(
            getCities.execute(),
            getGenders.execute(),
            getFigures.execute(),
            getEthnicities.execute(),
            getMaritalStatuses.execute(),
            getReligions.execute()
        ) { cities, genders, figures, ethnicities, ms, religions ->

            ProfileAttributes(
                cities = cities,
                genders = genders,
                figures = figures,
                ethnicities = ethnicities,
                maritalStatuses = ms,
                religions = religions
            )
        }
    }
}

class GetProfile internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(id: Long): Observable<Profile> {

        return profileApiService.getProfileById(id)
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class CreateProfile internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(profile: Profile): Observable<Long> {

        return profileApiService.createProfile(profile)
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class UpdateProfile internal constructor(
    private val dispatchers: Dispatchers,
    private val profileApiService: ProfileApiService
) {

    fun execute(id: Long, profile: Profile): Observable<Unit> {

        return profileApiService.updateProfile(id, profile)
            .unBox()
            .subscribeOn(dispatchers.io())
            .observeOn(dispatchers.ui())
    }
}

class UpdateProfilePicture internal constructor(
    private val pictureUploader: PictureUploader
) {

    fun execute(id: Long, file: File, mimeType: String): Observable<Picture> {

        return pictureUploader.upload(id, file, mimeType)
    }
}