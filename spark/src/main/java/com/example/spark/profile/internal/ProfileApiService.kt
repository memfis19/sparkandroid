package com.example.spark.profile.internal

import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Picture
import com.example.spark.profile.Profile
import com.example.spark.profile.Religion
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.adapter.rxjava2.Result
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import java.io.File

internal interface ProfileApiService {

    @POST("/profiles")
    fun createProfile(@Body profile: Profile): Observable<Result<Long>>

    @PUT("/profiles/{id}")
    fun updateProfile(@Path("id") profileId: Long, @Body profile: Profile): Observable<Result<Unit>>

    @Multipart
    @POST("/profiles/{id}/pictures")
    fun updateProfilePicture(
        @Path("id") profileId: Long,
        @Part file: MultipartBody.Part,
        @Part mimeType: MultipartBody.Part
    ): Observable<Result<Picture>>

    @GET("/profiles/{id}")
    fun getProfileById(@Path("id") profileId: Long): Observable<Result<Profile>>

    @GET("/cities")
    fun getCities(): Observable<Result<List<City>>>

    @GET("/genders")
    fun getGenders(): Observable<Result<List<Gender>>>

    @GET("/ethnicities")
    fun getEthnicities(): Observable<Result<List<Ethnicity>>>

    @GET("/figures")
    fun getFigures(): Observable<Result<List<Figure>>>

    @GET("/maritalStatuses")
    fun getMaritalStatuses(): Observable<Result<List<MaritalStatus>>>

    @GET("/religions")
    fun getReligions(): Observable<Result<List<Religion>>>
}