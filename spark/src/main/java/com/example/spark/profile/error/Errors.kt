package com.example.spark.profile.error

import com.example.spark.R
import com.example.spark.ui.TextResource

/***
* @param textResource - At the moment is hardcoded. But may be localized string and/or server response.
* */
sealed class SparkError(val textResource: TextResource) : Exception()

data class Unhandled(val error: Throwable) : SparkError(TextResource.fromStringId((R.string.general_error)))
object GeneralError : SparkError(TextResource.fromStringId((R.string.general_error)))
object BadRequest : SparkError(TextResource.fromStringId((R.string.general_error)))
object NotExist : SparkError(TextResource.fromStringId((R.string.general_error)))
object NotValid : SparkError(TextResource.fromStringId((R.string.general_error)))

sealed class Ui(textResource: TextResource) : SparkError(textResource) {
    object BadDisplayName : Ui(TextResource.fromStringId(R.string.display_name_empty))
    object BadRealName : Ui(TextResource.fromStringId(R.string.real_name_empty))
    object BadHeight : Ui(TextResource.fromStringId(R.string.height_value_incorrect))
}