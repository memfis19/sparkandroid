package com.example.spark.profile

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.Date

data class City(
    val id: Long,
    val city: String,
    @JsonProperty("lat") val latitude: String = "",
    @JsonProperty("lon") val longitude: String = ""
) {

    override fun toString(): String {
        return city
    }
}

data class Gender(val id: String, val name: String) {
    override fun toString(): String {
        return name
    }
}

data class Ethnicity(val id: String, val name: String) {
    override fun toString(): String {
        return name
    }
}

data class Figure(val id: String, val name: String) {
    override fun toString(): String {
        return name
    }
}

data class MaritalStatus(val id: String, val name: String) {
    override fun toString(): String {
        return name
    }
}

data class Religion(val id: String, val name: String) {
    override fun toString(): String {
        return name
    }
}

data class ProfileAttributes(
    val cities: List<City>,
    val genders: List<Gender>,
    val ethnicities: List<Ethnicity>,
    val figures: List<Figure>,
    val maritalStatuses: List<MaritalStatus>,
    val religions: List<Religion>
)

data class Picture(
    val guid: String,
    val mimeType: String
) {

    internal fun toMap(): Map<String, String> {

        return mapOf(
            Picture::guid.name to guid,
            Picture::mimeType.name to mimeType
        )
    }

    companion object {

        internal fun fromMap(map: Map<String, Any>): Picture? {

            return Picture(
                guid = map[Picture::guid.name]?.toString() ?: return null,
                mimeType = map[Picture::mimeType.name]?.toString() ?: return null
            )
        }
    }
}

data class Profile(
    val id: Long? = null,
    val picture: Picture? = null,
    val displayName: String,
    val realName: String,
    val birthdate: Date,
    val gender: Gender,
    val maritalStatus: MaritalStatus,
    val location: City,
    val profilePicture: String = "",
    val figure: Figure? = null,
    val height: Double = 0.0,
    val ethnicity: Ethnicity? = null,
    val religion: Religion? = null,
    val occupation: String = "",
    val aboutMe: String = ""
)