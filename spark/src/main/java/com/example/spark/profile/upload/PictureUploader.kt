package com.example.spark.profile.upload

import com.example.spark.profile.Picture
import io.reactivex.Observable
import java.io.File

interface PictureUploader {

    fun upload(id: Long, file: File, mimeType: String): Observable<Picture>
}

