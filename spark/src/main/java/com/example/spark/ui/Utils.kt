package com.example.spark.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Shader
import android.net.Uri
import android.provider.MediaStore
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import java.io.File

fun Uri.file(context: Context): File? {

    val projection = arrayOf(
        MediaStore.Images.ImageColumns.DISPLAY_NAME,
        MediaStore.Images.ImageColumns.MIME_TYPE
    )

    val cursor = context.contentResolver.query(this, projection, null, null, null)
    val fileNameAndMimeType = cursor?.use { nonNull ->
        val displayNameIndex = nonNull.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
        val mimeTypeIndex = nonNull.getColumnIndexOrThrow(MediaStore.Images.Media.MIME_TYPE)
        cursor.moveToFirst()
        val fileName = cursor.getString(displayNameIndex)
        val mimeType = cursor.getString(mimeTypeIndex)
        Pair(fileName, mimeType)
    } ?: return null


    return context.externalCacheDir?.let { path ->

        val file = File(path.path + "/" + fileNameAndMimeType.first)
        file.createNewFile()
        context.contentResolver.openInputStream(this)?.use { stream ->
            file.writeBytes(stream.readBytes())
        }

        return@let file
    } ?: return null
}

fun ImageView.setUrl(url: String) {

    Picasso.get()
        .load(url)
        .transform(CircleTransform())
        .into(this)
}

private class CircleTransform : Transformation {

    override fun transform(source: Bitmap): Bitmap {

        val size: Int = source.width.coerceAtMost(source.height)

        val x: Int = (source.width - size) / 2
        val y: Int = (source.height - size) / 2

        val squaredBitmap: Bitmap = Bitmap.createBitmap(source, x, y, size, size)
        if (squaredBitmap !== source) {
            source.recycle()
        }
        val bitmap: Bitmap = Bitmap.createBitmap(size, size, source.config)

        val canvas = Canvas(bitmap)
        val paint = Paint()
        val shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        paint.shader = shader
        paint.isAntiAlias = true

        val radius = size / 2f
        canvas.drawCircle(radius, radius, radius, paint)
        squaredBitmap.recycle()

        return bitmap
    }

    override fun key(): String {
        return "circle"
    }
}