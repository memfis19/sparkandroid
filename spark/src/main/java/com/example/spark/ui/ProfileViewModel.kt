package com.example.spark.ui

import android.webkit.MimeTypeMap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.spark.configuration.ImageUrlResolver
import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Profile
import com.example.spark.profile.ProfileAttributes
import com.example.spark.profile.Religion
import com.example.spark.profile.error.SparkError
import com.example.spark.profile.error.Ui
import com.example.spark.profile.error.Unhandled
import com.example.spark.profile.interactor.CreateProfile
import com.example.spark.profile.interactor.GetProfile
import com.example.spark.profile.interactor.LoadProfileAttributes
import com.example.spark.profile.interactor.UpdateProfile
import com.example.spark.profile.interactor.UpdateProfilePicture
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.util.Date

class ProfileViewModel(
    private val getProfile: GetProfile,
    private val createProfile: CreateProfile,
    private val updateProfile: UpdateProfile,
    private val updateProfilePicture: UpdateProfilePicture,
    private val loadProfileAttributes: LoadProfileAttributes,
    private val imageUrlResolver: ImageUrlResolver
) : ViewModel() {

    sealed class State {

        object Idle : State()

        sealed class Ready(
            open val profile: Profile,
            open val profileAttributes: ProfileAttributes
        ) : State() {

            data class NotValid(
                val sparkError: SparkError,
                override val profile: Profile,
                override val profileAttributes: ProfileAttributes
            ) : Ready(profile, profileAttributes)

            data class Valid(
                override val profile: Profile,
                override val profileAttributes: ProfileAttributes
            ) : Ready(profile, profileAttributes)
        }

        data class Failed(val sparkError: SparkError) : State()
    }

    val state: MutableLiveData<State> = MutableLiveData<State>(State.Idle)

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun loadProfileWithAttributes(profileId: Long = 1) {

        loadProfileAttributes.execute()
            .concatMap { attributes ->
                getProfile.execute(profileId).map { profile ->

                    State.Ready.Valid(profile, attributes)
                }
            }.go { value -> state.postValue(value) }

    }

    fun updateProfilePicture(file: File) {

        when (val st = state.value) {

            is State.Ready -> {

                val mimeType = file.resolverMimeType()

                st.profile.id?.let { id ->

                    updateProfilePicture.execute(id, file, mimeType).go { picture ->

                        val newProfile = st.profile.copy(picture = picture)
                        state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
                    }
                }
            }
        }
    }

    fun updateDisplayName(displayName: String) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(displayName = displayName)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateRealName(realName: String) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(realName = realName)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateBirthday(birthdate: Date) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(birthdate = birthdate)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateOccupation(occupation: String) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(occupation = occupation)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateAboutMe(aboutMe: String) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(aboutMe = aboutMe)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateGender(gender: Gender) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(gender = gender)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateMaritalStatus(maritalStatus: MaritalStatus) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(maritalStatus = maritalStatus)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateReligion(religion: Religion) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(religion = religion)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateFigure(figure: Figure) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(figure = figure)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateEthnicity(ethnicity: Ethnicity) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(ethnicity = ethnicity)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun updateCity(city: City) {

        when (val st = state.value) {

            is State.Ready -> {

                val newProfile = st.profile.copy(location = city)
                state.postValue(State.Ready.Valid(newProfile, st.profileAttributes))
            }
        }
    }

    fun applyProfileChanges() {

        when (val st = state.value) {
            is State.Ready -> {

                verifyProfileState(st)
            }
        }
    }

    fun resolveUrl(guid: String): String {

        return imageUrlResolver.resolveUrl(guid)
    }

    private fun verifyProfileState(profileState: State.Ready) {

        val profile = profileState.profile
        val profileAttributes = profileState.profileAttributes

        if (profile.displayName.isEmpty()) {

            state.postValue(
                State.Ready.NotValid(
                    sparkError = Ui.BadDisplayName,
                    profile = profile,
                    profileAttributes = profileAttributes
                )
            )
            return
        }
        if (profile.realName.isEmpty()) {

            state.postValue(
                State.Ready.NotValid(
                    sparkError = Ui.BadRealName,
                    profile = profile,
                    profileAttributes = profileAttributes
                )
            )
            return
        }
        if (profile.height <= 0) {

            state.postValue(
                State.Ready.NotValid(
                    sparkError = Ui.BadHeight,
                    profile = profile,
                    profileAttributes = profileAttributes
                )
            )
            return
        }

        val profileId: Long? = profile.id

        if (profileId == null) {

            createProfile.execute(profile).go()
        } else {

            updateProfile.execute(profileId, profile).go()
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun File.resolverMimeType(): String {

        return try {
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension) ?: ""
        } catch (error: Exception) {
            ""
        }
    }

    private fun <T> Observable<T>.go(success: (value: T) -> Unit = {}) {

        val disposable = subscribe(
            { value ->

                success(value)
            },
            { error ->

                val sparkError: SparkError = if (error is SparkError) {

                    error
                } else {

                    Unhandled(error)
                }

                when (val currentState = state.value) {
                    is State.Ready -> {

                        state.postValue(
                            State.Ready.NotValid(
                                sparkError = sparkError,
                                profile = currentState.profile,
                                profileAttributes = currentState.profileAttributes
                            )
                        )
                    }

                    else -> {

                        state.postValue(State.Failed(sparkError))
                    }
                }
            }
        )
        compositeDisposable.add(disposable)
    }
}