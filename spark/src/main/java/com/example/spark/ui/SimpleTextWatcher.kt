package com.example.spark.ui

import android.text.Editable
import android.text.TextWatcher

fun interface AfterTextChangedHandler {
    fun handle(text: String)
}

class SimpleTextWatcher(private val handler: AfterTextChangedHandler): TextWatcher {

    override fun beforeTextChanged(charSequence: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence?, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(editable: Editable?) {

        val value = editable?.toString()
        value?.let { nonNull ->
            handler.handle(nonNull)
        }
    }
}