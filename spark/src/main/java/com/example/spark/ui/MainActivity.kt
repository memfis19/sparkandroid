package com.example.spark.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.spark.R
import com.example.spark.databinding.ActivityMainBinding
import com.example.spark.profile.City
import com.example.spark.profile.Ethnicity
import com.example.spark.profile.Figure
import com.example.spark.profile.Gender
import com.example.spark.profile.MaritalStatus
import com.example.spark.profile.Picture
import com.example.spark.profile.Religion
import com.example.spark.profile.error.SparkError
import com.example.spark.profile.error.Ui
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


class MainActivity : AppCompatActivity() {

    private lateinit var viewsBinding: ActivityMainBinding

    private val dateFormat: DateFormat by lazy {
        SimpleDateFormat("dd.MM.yyyy", Locale.ROOT)
    }
    private val model: ProfileViewModel by viewModel()

    private val displayNameObserver = SimpleTextWatcher { value ->
        model.updateDisplayName(value)
    }

    private val realNameObserver = SimpleTextWatcher { value ->
        model.updateRealName(value)
    }

    private val occupationObserver = SimpleTextWatcher { value ->
        model.updateOccupation(value)
    }
    private val aboutMeObserver = SimpleTextWatcher { value ->
        model.updateAboutMe(value)
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {

                val file: File? = result.data?.data?.file(this)
                if (file != null) {
                    model.updateProfilePicture(file)
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewsBinding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(viewsBinding.root)

        viewsBinding.profilePicture.setOnClickListener {

            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(Intent.createChooser(intent, "Select Picture"))
        }

        val nameObserver = Observer<ProfileViewModel.State> { state ->

            when (state) {
                is ProfileViewModel.State.Idle -> {

                    model.loadProfileWithAttributes()
                }
                is ProfileViewModel.State.Ready.Valid -> {

                    bindState(state)
                }
                is ProfileViewModel.State.Ready.NotValid -> {

                    bindState(state)
                    handleError(state.sparkError)
                }
                is ProfileViewModel.State.Failed -> {

                    handleError(state.sparkError)
                }
            }
        }

        viewsBinding.done.setOnClickListener {

            model.applyProfileChanges()
        }

        model.state.observe(this, nameObserver)
    }

    private fun handleError(error: SparkError) {

        when (error) {
            Ui.BadDisplayName -> {
                viewsBinding.displayName.error = error.textResource.asString(resources)
            }
            Ui.BadRealName -> {
                viewsBinding.realName.error = error.textResource.asString(resources)
            }
            Ui.BadHeight -> {
                viewsBinding.height.error = error.textResource.asString(resources)
            }
            else -> {

                Snackbar.make(
                    viewsBinding.root,
                    error.textResource.asString(resources),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun bindState(state: ProfileViewModel.State.Ready) {

        bindDisplayName(state.profile.displayName)
        bindRealName(state.profile.realName)
        setProfilePicture(state.profile.picture)
        bindOccupation(state.profile.occupation)
        bindAboutMe(state.profile.aboutMe)
        bindBirthDate(state.profile.birthdate)
        bindHeight(state.profile.height)
        bindGender(state.profile.gender, state.profileAttributes.genders)
        bindMaritalStatus(
            state.profile.maritalStatus,
            state.profileAttributes.maritalStatuses
        )
        bindFigure(state.profile.figure, state.profileAttributes.figures)
        bindReligion(state.profile.religion, state.profileAttributes.religions)
        bindEthnicity(state.profile.ethnicity, state.profileAttributes.ethnicities)
        bindCities(state.profile.location, state.profileAttributes.cities)
    }

    private fun setProfilePicture(picture: Picture?) {

        if (picture != null) {

            viewsBinding.profilePicture.setUrl(model.resolveUrl(picture.guid))
        } else {

            viewsBinding.profilePicture.setImageResource(R.drawable.ic_avatar)
        }
    }

    private fun bindDisplayName(displayName: String) {

        viewsBinding.displayName.removeTextChangedListener(displayNameObserver)
        viewsBinding.displayName.setText(displayName)
        viewsBinding.displayName.setSelection(displayName.length)
        viewsBinding.displayName.addTextChangedListener(displayNameObserver)
    }

    private fun bindRealName(realName: String) {

        viewsBinding.realName.removeTextChangedListener(realNameObserver)
        viewsBinding.realName.setText(realName)
        viewsBinding.realName.setSelection(realName.length)
        viewsBinding.realName.addTextChangedListener(realNameObserver)
    }

    private fun bindHeight(height: Double) {

        viewsBinding.height.isEnabled = height == 0.0
        viewsBinding.height.setText(height.toString())
    }

    private fun bindBirthDate(birthdate: Date) {

        viewsBinding.birthdayDate.setText(dateFormat.format(birthdate))
        viewsBinding.birthdayDate.setOnClickListener {
            val calendar = Calendar.getInstance().apply {
                time = birthdate
            }
            val datePicker = DatePickerDialog(
                this,
                { _, year, month, dayOfMonth ->

                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, month)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    model.updateBirthday(calendar.time)
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.show()
        }
    }

    private fun bindOccupation(occupation: String) {

        viewsBinding.occupation.removeTextChangedListener(occupationObserver)
        viewsBinding.occupation.setText(occupation)
        viewsBinding.occupation.addTextChangedListener(occupationObserver)
    }

    private fun bindAboutMe(aboutMe: String) {

        viewsBinding.aboutMe.removeTextChangedListener(aboutMeObserver)
        viewsBinding.aboutMe.setText(aboutMe)
        viewsBinding.aboutMe.addTextChangedListener(aboutMeObserver)
    }

    private fun bindGender(gender: Gender, genders: List<Gender>) {

        val selectedIndex = if (genders.contains(gender)) genders.indexOf(gender) else 0

        viewsBinding.gender.setText(gender.toString())
        viewsBinding.gender.setOnClickListener {
            setupSingleChoice(
                R.string.profile_dialog_title_gender,
                genders.toTypedArray(),
                selectedIndex
            ) { value ->

                model.updateGender(value)
            }
        }
    }

    private fun bindMaritalStatus(
        maritalStatus: MaritalStatus,
        maritalStatuses: List<MaritalStatus>
    ) {

        val selectedIndex =
            if (maritalStatuses.contains(maritalStatus)) maritalStatuses.indexOf(maritalStatus) else 0

        viewsBinding.maritalStatus.setText(maritalStatus.toString())
        viewsBinding.maritalStatus.setOnClickListener {

            setupSingleChoice(
                R.string.profile_dialog_title_marital_status,
                maritalStatuses.toTypedArray(),
                selectedIndex
            ) { value ->

                model.updateMaritalStatus(value)
            }
        }
    }

    private fun bindFigure(figure: Figure?, figures: List<Figure>) {

        val selectedIndex = if (figures.contains(figure)) figures.indexOf(figure) else 0

        figure?.let { nonNull ->
            viewsBinding.figure.setText(nonNull.toString())
        }
        viewsBinding.figure.setOnClickListener {

            setupSingleChoice(
                R.string.profile_dialog_title_figure,
                figures.toTypedArray(),
                selectedIndex
            ) { value ->

                model.updateFigure(value)
            }
        }
    }

    private fun bindReligion(religion: Religion?, religions: List<Religion>) {

        val selectedIndex = if (religions.contains(religion)) religions.indexOf(religion) else 0

        religion?.let { nonNull ->
            viewsBinding.religion.setText(nonNull.toString())
        }
        viewsBinding.religion.setOnClickListener {

            setupSingleChoice(
                R.string.profile_dialog_title_religion,
                religions.toTypedArray(),
                selectedIndex
            ) { value ->

                model.updateReligion(value)
            }
        }
    }

    private fun bindEthnicity(ethnicity: Ethnicity?, ethnicities: List<Ethnicity>) {

        val selectedIndex =
            if (ethnicities.contains(ethnicity)) ethnicities.indexOf(ethnicity) else 0

        ethnicity?.let { nonNull ->
            viewsBinding.ethnicity.setText(nonNull.toString())
        }
        viewsBinding.ethnicity.setOnClickListener {

            setupSingleChoice(
                R.string.profile_dialog_title_ethnicity,
                ethnicities.toTypedArray(),
                selectedIndex
            ) { value ->

                model.updateEthnicity(value)
            }
        }
    }

    private fun bindCities(city: City, cities: List<City>) {

        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            cities.toTypedArray()
        )
        viewsBinding.location.setAdapter(adapter)
        viewsBinding.location.setText(city.city)
        viewsBinding.location.setSelection(city.city.length)
        viewsBinding.location.setOnItemClickListener { _, _, position, _ ->

            adapter.getItem(position)?.let { location ->

                model.updateCity(location)
            }
        }
    }

    private fun <T> setupSingleChoice(
        @StringRes title: Int,
        elements: Array<T>,
        index: Int = 0,
        onItemSelected: (element: T) -> Unit
    ) {

        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            elements
        )
        AlertDialog.Builder(this)
            .setTitle(title)
            .setSingleChoiceItems(adapter, index) { dialog, position ->

                adapter.getItem(position)?.let(onItemSelected)

                dialog.dismiss()
            }.show()
    }
}