package com.example.spark.foundation

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface Dispatchers {

    fun io(): Scheduler

    fun computation(): Scheduler

    fun ui(): Scheduler
}

internal object RxDispatchers : Dispatchers {

    override fun io(): Scheduler = Schedulers.io()

    override fun computation(): Scheduler = Schedulers.computation()

    override fun ui(): Scheduler = AndroidSchedulers.mainThread()
}