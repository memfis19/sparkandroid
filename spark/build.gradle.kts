plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdkVersion(30)
    buildToolsVersion = "30.0.3"

    defaultConfig {
        applicationId = "com.example.spark"
        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("debug") {
        }
    }
    flavorDimensions("deploy")
    productFlavors {
        create("custom") {
            dimension = "deploy"
            buildConfigField("String", "DEPLOY_URL", "\"${deployUrl("http://192.168.88.61:8099/")}\"")
        }
        create("local") {
            dimension = "deploy"
            buildConfigField("String", "DEPLOY_URL", "\"http://localhost/\"")
        }
        create("public") {
            dimension = "deploy"
            buildConfigField(
                "String",
                "DEPLOY_URL",
                "\"https://evening-everglades-39342.herokuapp.com/\""
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    testOptions {
        animationsDisabled = true
        unitTests.isIncludeAndroidResources = true
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {

    implementation(Dependencies.Kotlin.kotlinStdLib)

    implementation(Dependencies.Android.androidXCore)
    implementation(Dependencies.Android.androidXAppCompat)
    implementation(Dependencies.Android.material)

    implementation(Dependencies.Android.androidXActivity)
    implementation(Dependencies.Android.androidXFragment)
    implementation(Dependencies.Android.androidXWorkManager)
    implementation(Dependencies.Android.androidXWorkManagerRx2)

    implementation(Dependencies.Koin.koinAndroid)
    implementation(Dependencies.Koin.koinScopeAndroid)
    implementation(Dependencies.Koin.koinViewModel)

    implementation(Dependencies.Rx.rxJava2)
    implementation(Dependencies.Rx.rxJavaAndroid2)

    implementation(platform(Dependencies.Network.okHttpPlatform))
    implementation(Dependencies.Network.okHttp)
    implementation(Dependencies.Network.okHttpInterceptor)
    implementation(Dependencies.Network.retrofit)
    implementation(Dependencies.Network.retrofitRxAdapter)
    implementation(Dependencies.Network.retrofitJacksonConverter)
    implementation(Dependencies.Network.retrofitScalarsConverter)
    implementation(Dependencies.Network.jacksonKotlin)

    implementation(Dependencies.Utils.picasso)

    testImplementation(Dependencies.Test.junit)
    testImplementation(Dependencies.Test.androidCoreTesting)
    testImplementation(Dependencies.Test.androidXTestCore)
    testImplementation(Dependencies.Test.mockitoCore)
    testImplementation(Dependencies.Test.mockitoKotlin)
    testImplementation(Dependencies.Test.assertJ)
    testImplementation(Dependencies.Koin.koinTest)

    androidTestImplementation(Dependencies.Test.androidXJunit)
    androidTestImplementation(Dependencies.Test.androidXEspresso)
    androidTestImplementation(Dependencies.Test.androidXEspressoIdling)
    androidTestImplementation(Dependencies.Test.mockitoKotlin)
    androidTestImplementation(Dependencies.Test.mockitoAndroid)
    androidTestImplementation(Dependencies.Koin.koinTest)
}